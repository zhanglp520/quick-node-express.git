## /后台管理系统api-v1.0.0
```text
接口v1.0.0
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
Authorization | Bearer {{token}} | -
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理
```text
暂无描述
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/用户管理
```text
用户操作相关接口
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/用户管理/用户分页列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/getPageList?current=2&size=10

#### 请求方式
> GET

#### Content-Type
> none

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
current | 2 | String | 是 | 当前页码
size | 10 | String | 是 | 每页条数
userName | zhanglp | String | 否 | 用户名
startTime | 2022-07-13 | String | 否 | 开始时间
endTime | 2022-07-14 | String | 否 | 结束时间
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "订单分页列表",
	"data": [
		{
			"id": 11,
			"user_id": "YH_1",
			"user_name": "admin",
			"password": "14e1b600b1fd579f47433b88e8d85291",
			"avatar": "",
			"full_name": "管理员",
			"phone": "15229380174",
			"email": "quick@163.com",
			"address": "北京朝阳",
			"create_time": "2022-05-13 16:20:47",
			"remark": "管理员账号，请勿删除",
			"deleted": 0,
			"enabled": 0
		}
	],
	"page": {
		"current": 1,
		"size": 10,
		"total": 3
	}
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 订单分页列表 | String | 提示信息
data | - | Object | 响应数据
data.id | 11 | Number | 订单主键
data.user_id | YH_1 | String | 用户编号
data.user_name | admin | String | 用户名
data.password | 14e1b600b1fd579f47433b88e8d85291 | String | 密码
data.avatar | - | Object | 头像
data.full_name | 管理员 | String | 姓名
data.phone | 15229380174 | String | 手机号
data.email | quick@163.com | String | 邮箱
data.address | 北京朝阳 | String | 地址
data.create_time | 2022-05-13 16:20:47 | String | 创建时间
data.remark | 管理员账号，请勿删除 | String | 备注
data.deleted | - | Number | 是否删除：0：未删除，1：已删除
data.enabled | - | Number | 是否启用：0：启用，1：禁用
page | - | Object | 分页
page.current | 1 | Number | 当前页码
page.size | 10 | Number | 每页条数
page.total | 3 | Number | 分页总条数
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "获取用户分页列表失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 获取用户分页列表失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/用户列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/getList

#### 请求方式
> GET

#### Content-Type
> none

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "订单分页列表",
	"data": [
		{
			"id": 11,
			"user_id": "YH_1",
			"user_name": "admin",
			"password": "14e1b600b1fd579f47433b88e8d85291",
			"avatar": "",
			"full_name": "管理员",
			"phone": "15229380174",
			"email": "quick@163.com",
			"address": "北京朝阳",
			"create_time": "2022-05-13 16:20:47",
			"remark": "管理员账号，请勿删除",
			"deleted": 0,
			"enabled": 0
		}
	],
	"page": {
		"current": 1,
		"size": 10,
		"total": 3
	}
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 订单分页列表 | String | 提示信息
data | - | Object | 响应数据
data.id | 11 | Number | 订单主键
data.user_id | YH_1 | String | 用户编号
data.user_name | admin | String | 用户名
data.password | 14e1b600b1fd579f47433b88e8d85291 | String | 密码
data.avatar | - | Object | 头像
data.full_name | 管理员 | String | 姓名
data.phone | 15229380174 | String | 手机号
data.email | quick@163.com | String | 邮箱
data.address | 北京朝阳 | String | 地址
data.create_time | 2022-05-13 16:20:47 | String | 创建时间
data.remark | 管理员账号，请勿删除 | String | 备注
data.deleted | - | Number | 是否删除：0：未删除，1：已删除
data.enabled | - | Number | 是否启用：0：启用，1：禁用
page | - | Object | 分页
page.current | 1 | Number | 当前页码
page.size | 10 | Number | 每页条数
page.total | 3 | Number | 分页总条数
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "获取用户分页列表失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 获取用户分页列表失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/用户详情
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/getDetail?id=45

#### 请求方式
> GET

#### Content-Type
> none

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 45 | Text | 是 | 订单主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "用户详情",
	"data": {
		"id": 11,
		"user_id": "YH_1",
		"user_name": "admin",
		"password": "14e1b600b1fd579f47433b88e8d85291",
		"avatar": "",
		"full_name": "管理员",
		"phone": "15229380174",
		"email": "quick@163.com",
		"address": "北京朝阳",
		"create_time": "2022-05-13 16:20:47",
		"remark": "管理员账号，请勿删除",
		"deleted": 0,
		"enabled": 0
	}
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 用户详情 | String | 提示信息
data | - | Object | 响应数据
data.id | 11 | Number | 订单主键
data.user_id | YH_1 | String | 用户编号
data.user_name | admin | String | 用户名
data.password | 14e1b600b1fd579f47433b88e8d85291 | String | 密码
data.avatar | - | Object | 头像
data.full_name | 管理员 | String | 姓名
data.phone | 15229380174 | String | 手机号
data.email | quick@163.com | String | 邮箱
data.address | 北京朝阳 | String | 地址
data.create_time | 2022-05-13 16:20:47 | String | 创建时间
data.remark | 管理员账号，请勿删除 | String | 备注
data.deleted | - | Number | 是否删除：0：未删除，1：已删除
data.enabled | - | Number | 是否启用：0：启用，1：禁用
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "获取用户详情失败",
	"data": null,
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 获取用户详情失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/登录用户信息
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/getInfo?userName=admin

#### 请求方式
> GET

#### Content-Type
> none

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
userName | admin | Text | 是 | 用户名
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "登录用户信息",
	"data": {
		"id": 11,
		"user_id": "YH_1",
		"user_name": "admin",
		"password": "14e1b600b1fd579f47433b88e8d85291",
		"avatar": "",
		"full_name": "管理员",
		"phone": "15229380174",
		"email": "quick@163.com",
		"address": "北京朝阳",
		"create_time": "2022-05-13 16:20:47",
		"remark": "管理员账号，请勿删除",
		"deleted": 0,
		"enabled": 0
	}
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 登录用户信息 | String | 提示信息
data | - | Object | 响应数据
data.id | 11 | Number | 订单主键
data.user_id | YH_1 | Number | 用户编号
data.user_name | admin | String | 用户名称
data.password | 14e1b600b1fd579f47433b88e8d85291 | String | 密码
data.avatar | - | Object | 头像
data.full_name | 管理员 | String | 姓名
data.phone | 15229380174 | String | 手机号
data.email | quick@163.com | String | 邮箱
data.address | 北京朝阳 | String | 地址
data.create_time | 2022-05-13 16:20:47 | String | 创建时间
data.remark | 管理员账号，请勿删除 | String | 备注
data.deleted | - | Number | 是否删除：0：未删除，1：已删除
data.enabled | - | Number | 是否启用：0：启用，1：禁用
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "获取用户登录信息失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 获取用户登录信息失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/新增用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/add

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"userId": "YH_333",
	"userName": "leifn22",
	"avatar": "",
	"fullName": "",
	"phone": "",
	"email": "1841031740@qq.com",
	"address": "陕西西安"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
userId | YH_333 | String | 是 | 用户编号
userName | leifn22 | String | 是 | 用户名
avatar | /upload/1111.png | String | 否 | 头像
fullName | 张飞 | String | 否 | 姓名
phone | 15229380174 | String | 否 | 手机号
email | 1841031740@qq.com | String | 否 | 邮箱
address | 陕西西安 | String | 否 | 地址
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "添加用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 添加用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "添加用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 添加用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/编辑用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/update

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": "41",
	"userName": "lllll",
	"avatar": "",
	"fullName": "张立平",
	"phone": "13520073575",
	"email": "1841031740@qq.com",
	"address": "陕西西安2"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 41 | String | 是 | 订单主键
userName | lllll | String | 是 | 用户名
avatar | /uploads/22.png | String | 否 | 头像
fullName | 张立平 | String | 否 | 姓名
phone | 13520073575 | String | 否 | 手机号
email | 1841031740@qq.com | String | 否 | 邮箱
address | 陕西西安2 | String | 否 | 地址
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "修改用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 修改用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "修改用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 修改用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/删除用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/remove

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"41"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 订单主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "删除用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 删除用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "删除用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 删除用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/批量删除用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/batchRemove

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "ids":"35,36,37"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
ids | 4,5,6 | String | 是 | 需要删除的主键，多个以逗号分割
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "删除用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 删除用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "删除用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 删除用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/批量导入用户
```text
暂无描述
```
#### 接口状态
> 需修改

#### 接口URL
> /user/improtUser

#### 请求方式
> POST

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/用户管理/批量导出用户
```text
暂无描述
```
#### 接口状态
> 需修改

#### 接口URL
> /user/exportUser

#### 请求方式
> POST

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/用户管理/重置用户密码
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/resetPassword

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"4"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 订单主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "重置用户密码成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 重置用户密码成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "重置用户密码失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 重置用户密码失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/启用用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/enabled

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"45"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 订单主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "启用用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 启用用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "启用用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 启用用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/用户管理/禁用用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/disable

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"45"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 订单主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "禁用用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 禁用用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "禁用用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 禁用用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/角色管理
```text
角色相关操作
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/角色管理/角色列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/getList

#### 请求方式
> GET

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/角色管理/新增角色
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/add

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": "",
	"roleId": "test1",
	"roleName": "测试角色"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/角色管理/删除角色
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/delete

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"7"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 角色主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "删除用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 删除用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "删除用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 删除用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/角色管理/修改角色
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/update

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": "7",
	"roleId": "test1",
	"roleName": "测试角色222"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/部门管理
```text
系统部门相关接口
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/部门管理/部门列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dept/getList

#### 请求方式
> GET

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/部门管理/获取父级下的部门列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dept/getListByPid?pId=20

#### 请求方式
> GET

#### Content-Type
> form-data

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
pId | 20 | Text | 是 | -
#### 请求Body参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
token | Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImFkbWluIiwiaWF0IjoxNjU5MjE3MTc3LCJleHAiOjE2NTk0NzYzNzd9.-ZRbujbPYbMMbg3oY-HiLraB5B-B-2tjU1gDwfTkYls | Text | 是 | 认证令牌
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/部门管理/新增部门
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dept/add

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"deptId": "BM_0008",
	"name": "运营部",
	"pid": 22
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/部门管理/编辑部门
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dept/update

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": 28,
	"name": "运营部备份1",
	"pid":22
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei11 | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/部门管理/删除部门
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dept/delete

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": 7,
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 41 | String | 是 | 字典主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/菜单管理
```text
菜单相关操作
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/菜单管理/菜单列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /menu/getList

#### 请求方式
> GET

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/菜单管理/新增菜单
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /menu/add

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
"menuName": "test-menu",
	"path": "user",
	"menuType": 1,
	"icon": "",
	"sort": 4,
	"pid": 1,
	"link": 0,
	"enabled": 0,
	"status": 0
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/菜单管理/删除菜单
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /menu/delete

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"41"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 菜单主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "删除用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 删除用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "删除用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 删除用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/系统管理/菜单管理/修改菜单
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /menu/update

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": 15,
	"menuId": "001",
	"menuName": "test-menu",
	"path": "user",
	"menuType": 1,
	"icon": "",
	"sort": 4,
	"pid": 1,
	"link": 0,
	"enabled": 0,
	"status": 0
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典管理
```text
系统字典相关接口
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典管理/字典列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionary/getList?dic_type_id=gender

#### 请求方式
> GET

#### Content-Type
> form-data

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
dic_type_id | gender | Text | 是 | 字典类型
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典管理/新增字典
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionary/add

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id":"",
	"dic_type_id": "keyword",
	"dic_id": "14",
	"name": "联系我"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典管理/编辑字典
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionary/update

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id":"15",
	"dic_type_id": "keyword",
	"dic_id": "14",
	"name": "联系我"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei11 | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典管理/删除字典
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionary/delete

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"41"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 41 | String | 是 | 字典主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典分类
```text
字典分类相关接口
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典分类/字典分类列表
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionaryType/getList

#### 请求方式
> GET

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典分类/新增字典分类
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionaryType/add

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": "",
	"dic_type_id": "gender1",
	"name": "性别1"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典分类/编辑字典分类
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionaryType/update

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"id": 1,
	"dic_type_id": "gender1",
	"name": "性别3"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei11 | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/字典分类/删除字典分类
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /dictionaryType/delete

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"41"
}
```
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/日志管理
```text
日志相关操作
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/日志管理/日志分页列表
```text
暂无描述
```
#### 接口状态
> 开发中

#### 接口URL
> /log/getPageList?startTime=1659542400&endTime=1659542400&current=1&size=10&logType=1

#### 请求方式
> GET

#### Content-Type
> form-data

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
startTime | 1659542400 | Text | 是 | 1659542400
endTime | 1659542400 | Text | 是 | 1659542400
current | 1 | Text | 是 | -
size | 10 | Text | 是 | -
logType | 1 | Text | 是 | 0:操作日志，1：错误日志
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/日志管理/删除日志
```text
暂无描述
```
#### 接口状态
> 开发中

#### 接口URL
> /log/remove

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "id":"7"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/系统管理/日志管理/批量删除日志
```text
暂无描述
```
#### 接口状态
> 开发中

#### 接口URL
> /log/batchRemove

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
    "ids":"7,8"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 4 | String | 是 | 角色主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "删除用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 删除用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "删除用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 删除用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/权限管理
```text
暂无描述
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/权限管理/用户权限
```text
系统字典相关接口
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/权限管理/用户权限/获取用户权限
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /user/getPermission?id=62

#### 请求方式
> GET

#### Content-Type
> json

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 62 | Text | 是 | 用户主键
#### 请求Body参数
```javascript

```
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
#### 成功响应示例
```javascript
{
	"status": 0,
	"msg": "禁用用户成功",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | - | Number | 状态：0：成功，1：失败
msg | 禁用用户成功 | String | 提示信息
data | - | Object | 响应数据
#### 错误响应示例
```javascript
{
	"status": 1,
	"msg": "禁用用户失败",
	"data": null
}
```
参数名 | 示例值 | 参数类型 | 参数描述
--- | --- | --- | ---
status | 1 | Number | 状态：0：成功，1：失败
msg | 禁用用户失败 | String | 提示信息
data | - | Object | 响应数据
## /后台管理系统api-v1.0.0/权限管理/角色权限
```text
系统字典相关接口
```
#### 公共Header参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Query参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 公共Body参数
参数名 | 示例值 | 参数描述
--- | --- | ---
暂无参数
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/权限管理/角色权限/获取角色分配的菜单权限
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/getMenuPermission?id=9

#### 请求方式
> GET

#### Content-Type
> form-data

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 9 | Text | 是 | 角色主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/权限管理/角色权限/获取角色分配的用户权限
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/getUserPermission?id=1

#### 请求方式
> GET

#### Content-Type
> form-data

#### 请求Query参数
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 1 | Text | 是 | 角色主键
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/权限管理/角色权限/角色分配权限
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/assignPermission

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"roleId":"9",
	"menuIds": "8,9"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/权限管理/角色权限/角色分配用户
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /role/assignUser

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	"roleId":"9",
	"userIds": "1,2,3"
}
```
参数名 | 示例值 | 参数类型 | 是否必填 | 参数描述
--- | --- | --- | --- | ---
id | 5 | Number | 是 | -
username | lei | String | 是 | 用户名
password | 123 | String | 是 | 密码
createtime | 2022-05-13 | String | 是 | -
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/登录
```text
暂无描述
```
#### 接口状态
> 已完成

#### 接口URL
> /login

#### 请求方式
> POST

#### Content-Type
> json

#### 请求Body参数
```javascript
{
	
	"userName": "admin",
	"userPassword": "e10adc3949ba59abbe56e057f20f883e"
}
```
#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
const token=response.json.data.token
apt.variables.set("token", token);
```
## /后台管理系统api-v1.0.0/刷新token
```text
暂无描述
```
#### 接口状态
> 开发中

#### 接口URL
> /user/refreshToken

#### 请求方式
> POST

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```
## /后台管理系统api-v1.0.0/退出
```text
暂无描述
```
#### 接口状态
> 开发中

#### 接口URL
> /user/loginOut

#### 请求方式
> POST

#### Content-Type
> form-data

#### 预执行脚本
```javascript
暂无预执行脚本
```
#### 后执行脚本
```javascript
暂无后执行脚本
```