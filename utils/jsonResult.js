module.exports = {
  success: (msg, data = null, page = null) => {
    const obj = {
      status: 0,
      msg,
      data,
    };
    if (page) {
      obj["page"] = page;
    }
    return obj;
  },
  fail: (msg) => {
    const obj = {
      status: 1,
      msg,
      data: null,
    };
    return obj;
  },
  exception: (msg,err) => {
    const obj = {
      status: 1,
      msg,
      err,
      data: null,
    };
    return obj;
  },
};
