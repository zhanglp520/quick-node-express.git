const express = require("express");
const router = express.Router();
const {
  getUserPermission,
  getMenuPermission,
  getList,
  add,
  update,
  remove,
  assignMenu,
  assignUser,
} = require("../controllers/roleController");

router.get("/api/role/getUserPermission", (req, res) => {
  const { query } = req;
  getUserPermission(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取用户权限失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/role/getMenuPermission", (req, res) => {
  const { query } = req;
  getMenuPermission(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取菜单权限失败",
        status: 1,
      });
      next(err);
    });
});
router.get("/api/role/getList", (req, res) => {
  const { query } = req;
  getList(query)
    .then((data) => {
      console.log("data", data);
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取角色列表失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/role/add", (req, res) => {
  const { body } = req;
  add(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "添加角色失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/role/update", (req, res) => {
  const { body } = req;
  update(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "修改角色失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/role/delete", (req, res) => {
  const { body } = req;
  remove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除角色失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/role/assignPermission", (req, res) => {
  const { body } = req;
  assignMenu(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "分配权限失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/role/assignUser", (req, res) => {
  const { body } = req;
  assignUser(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "分配用户失败",
        status: 1,
      });
      next(err);
    });
});
module.exports = router;
