const express = require("express");
const router = express.Router();
const {
  getList,
  add,
  update,
  remove,
} = require("../controllers/dictionaryTypeController");

router.get("/api/dictionaryType/getList", (req, res) => {
  const { query } = req;
  getList(query)
    .then((data) => {
      console.log("data", data);
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取字典分类列表失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/dictionaryType/add", (req, res) => {
  const { body } = req;
  add(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "添加字典分类失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/dictionaryType/update", (req, res) => {
  const { body } = req;
  update(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "修改字典分类失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/dictionaryType/delete", (req, res) => {
  const { body } = req;
  remove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除字典失败",
        status: 1,
      });
      next(err);
    });
});

module.exports = router;
