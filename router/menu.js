const express = require("express");
const router = express.Router();
const {
  getList,
  add,
  update,
  remove,
} = require("../controllers/menuController");

router.get("/api/menu/getList", (req, res) => {
  const { query } = req;
  getList(query).then((data) => {
    console.log('data',data);
    res.send(data);
  }).catch((err)=>{
    res.send({
      data:null,
      msg:'菜单列表获取出错',
      status:1
    });
    next(err)
  });
});
router.post("/api/menu/add", (req, res,next) => {
  const { body } = req;
  add(body).then((data) => {
    console.log('1111111111111');
    res.send(data);
  }).catch((err)=>{
    console.log('2222222222222');
    res.send({
      data:null,
      msg:'添加菜单失败',
      status:1
    });
    next(err)
  });
});
router.post("/api/menu/update", (req, res,next) => {
  const { body } = req;
  update(body).then((data) => {
    res.send(data);
  }).catch((err)=>{
    next(err)
    res.send({
      data:null,
      msg:'修改菜单失败',
      status:1
    });
  });
});
router.post("/api/menu/delete", (req, res) => {
  const { body } = req;
  remove(body).then((data) => {
    res.send(data);
  }).catch((err)=>{
    res.send({
      data:null,
      msg:'删除菜单失败',
      status:1
    });
    next(err)
  });
});



module.exports = router;
