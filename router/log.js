const express = require("express");
const router = express.Router();
const {
  getPageList,
  remove,
  batchRemove,
} = require("../controllers/logController");

router.get("/api/log/getPageList", (req, res, next) => {
  const { query } = req;
  getPageList(query)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "获取日志分页列表失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/log/remove", (req, res, next) => {
  const { body } = req;
  remove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除日志失败",
        status: 1,
      });
      next(err);
    });
});
router.post("/api/log/batchRemove", (req, res, next) => {
  const { body } = req;
  batchRemove(body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send({
        data: null,
        msg: "删除日志失败",
        status: 1,
      });
      next(err);
    });
});
module.exports = router;
