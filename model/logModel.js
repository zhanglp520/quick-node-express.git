const db = require("../config/dbPool");
const { formatDate } = require("../utils/index");
class LogModel {
  selectPage(logType, current, size, startTime, endTime) {
    let whereStr = `log_type=${logType}`;
    if (startTime && endTime) {
      startTime = formatDate(startTime);
      endTime = formatDate(endTime);
      whereStr = `${whereStr} and log_time>='${startTime}' and log_time<'${endTime}'`;
    }

    const sql = `select * from sys_logs where ${whereStr} order by id desc limit ?,?`;
    const sqlArr = [current, size];
    return db.queryAsync(sql, sqlArr);
  }
  selectTotal(logType, startTime, endTime) {
    let whereStr = `log_type=${logType}`;
    if (startTime && endTime) {
      startTime = formatDate(startTime);
      endTime = formatDate(endTime);
      whereStr = `${whereStr} and log_time>='${startTime}' and log_time<'${endTime}'`;
    }

    const sql = `select count(id) as total from sys_logs where ${whereStr}`;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  insert(log) {
    const sql = `insert into sys_logs(log_time,operate_api,request_params,error_message,exception_message,log_type,ip) values(?,?,?,?,?,?,?)`;
    const sqlArr = [
      log.logTime,
      log.operateApi,
      log.requestParams,
      log.errorMessage,
      log.exceptionMessage,
      log.logType,
      log.ip,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(ids) {
    const sql = `delete from sys_logs where id in (${ids})`;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new LogModel();
