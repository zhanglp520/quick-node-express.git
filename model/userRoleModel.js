'use strict';
const db = require("../config/dbPool");

class UserRoleModel {
  selectWhere( field, value){
    const sql = `select * from per_user_roles where ${field}=?`;
    const sqlArr = [value];
    return db.queryAsync(sql, sqlArr);
  }
  insert(userRole) {
    const sql =
      "insert into per_user_roles(role_id,user_id) values(?,?)";
    const sqlArr = [
      userRole.roleId,
      userRole.userId,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  removeWhere(roleId) {
    const sql = "delete from per_user_roles where role_id=?";
    const sqlArr = [roleId];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new UserRoleModel();
