const db = require("../config/dbPool");
const { getDate } = require("../utils/index");

class DictionaryModel {
  selectWhere(dicTypeId) {
    const sql = "select * from sys_dictionaries where dic_type_id=?";
    const sqlArr = [dicTypeId];
    return db.queryAsync(sql, sqlArr);
  }
  insert(dic) {
    dic.create_time = getDate();
    const sql =
      "insert into sys_dictionaries(dic_type_id,dic_id,name) values(?,?,?)";
    const sqlArr = [
      dic.dicTypeId,
      dic.dicId,
      dic.name,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(dic) {
    const sql =
      "update sys_dictionaries set dic_type_id=?,dic_id=?,name=? where id=?";
    const sqlArr = [
      dic.dicTypeId,
      dic.dicId,
      dic.name,
      dic.id,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(id) {
    const sql = "delete from sys_dictionaries where id=?";
    const sqlArr = [id];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new DictionaryModel();
