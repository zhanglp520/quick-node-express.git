const { success } = require("../utils/jsonResult");
const {
  select,
  insert,
  update,
  remove,
} = require("../model/dictionaryTypeModel");

class DictionaryTypeServices {
  constructor() {}
  async getDictionaryTypeList() {
    try {
      const dictionaryTypeListResult = await select();
      const dictionaryTypeList =
        dictionaryTypeListResult.results && dictionaryTypeListResult.results;
      return success("字典分类列表",dictionaryTypeList);
    } catch (error) {
      throw error;
    }
  }
  async addDictionaryType(dictionaryType) {
    try {
      await insert(dictionaryType);
      return success("添加字典分类成功");
    } catch (error) {
      throw error;
    }
  }
  async updateDictionaryType(dictionaryType) {
    try {
      await update(dictionaryType);
      return success("修改字典分类成功");
    } catch (error) {
      throw error;
    }
  }
  async deleteDictionaryType(id) {
    try {
      await remove(id);
      return success("删除字典分类成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new DictionaryTypeServices();
