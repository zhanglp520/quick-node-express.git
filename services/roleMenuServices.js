const { success } = require("../utils/jsonResult");
const { selectWhere, removeWhere, insert } = require("../model/roleMenuModel");

class RoleMenuServices {
  constructor() {}
  async getRoleMenuListByRoleIds(roleIds) {
    try {
      const roleMenuListResult = await selectWhere(roleIds);
      const roleMenuList =
        roleMenuListResult.results && roleMenuListResult.results;
      return success("分配的菜单列表",roleMenuList);
    } catch (error) {
      throw error;
    }
  }
  async getAssignedUserList(userId) {
    try {
      const userRoleListResult = await selectWhere("user_id", userId);
      const userRoleList =
        userRoleListResult.results && userRoleListResult.results;
      return success("分配的用户列表",userRoleList);
    } catch (error) {
      throw error;
    }
  }
  async bindMenuForRole(roleId, menuIds) {
    try {
      await removeWhere(roleId);
      const menuIdArr = menuIds.split(",");
      menuIdArr.forEach(async (menuId) => {
        const roleMenu = { roleId, menuId };
        await insert(roleMenu);
      });
      return success("角色权限分配成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new RoleMenuServices();
