const { success } = require("../utils/jsonResult");
const {
  selectWhere,
  select,
  insert,
  update,
  remove,
} = require("../model/deptModel");

class DeptServices {
  constructor() {}
  async getDeptListByPId(pId) {
    try {
      const deptListResult = await selectWhere(pId);
      const deptList = deptListResult.results && deptListResult.results;
      return success("获取父级下的部门列表",deptList);
    } catch (error) {
      throw error;
    }
  }
  async getDeptList() {
    try {
      const deptListResult = await select();
      const deptList = deptListResult.results && deptListResult.results;
      return success("部门列表",deptList);
    } catch (error) {
      throw error;
    }
  }
  async addDept(dept) {
    try {
      await insert(dept);
      return success("添加部门成功");
    } catch (error) {
      throw error;
    }
  }
  async updateDept(dept) {
    try {
      await update(dept);
      return success("修改部门成功");
    } catch (error) {
      throw error;
    }
  }
  async deleteDept(id) {
    try {
      await remove(id);
      return success("删除部门成功");
    } catch (error) {
      throw error;
    }
  }
}
module.exports = new DeptServices();
