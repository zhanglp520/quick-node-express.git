const {
  getDictionaryList,
  addDictionary,
  updateDictionary,
  deleteDictionary,
} = require("../services/dictionaryServices");
class DictionaryController {
  constructor() {}
  getList(body) {
    const {dicTypeId}=body
       return getDictionaryList(dicTypeId)
  }
  add(body) {
    const dictionary = body;
    return addDictionary(dictionary);
  }
  update(body) {
    const dictionary = body;
    return updateDictionary(dictionary);
  }
  remove(body) {
    const { id } = body;
    return deleteDictionary(id);
  }
}
module.exports = new DictionaryController();
