const {
  getMenuList,
  addMenu,
  updateMenu,
  deleteMenu,
  getPermissionMenuListByUserId,
  getPermissionMenuListByRoleId,
  assignPermission,
} = require("../services/menuServices");

class MenuController {
  constructor() {}  
  getList() {
    return getMenuList();
  }
  add(body) {
    const menu = body;
    return addMenu(menu);
  }
  update(body) {
    const menu = body;
    return updateMenu(menu);
  }
  remove(body) {
    const { id } = body;
    return deleteMenu(id);
  }
}
module.exports = new MenuController();
