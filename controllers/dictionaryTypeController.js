const {
  getDictionaryTypeList,
  addDictionaryType,
  updateDictionaryType,
  deleteDictionaryType,
} = require("../services/dictionaryTypeServices");
class DictionaryTypeController {
  constructor() {}
  getList() {
    return getDictionaryTypeList();
  }
  add(body) {
    const dictionaryType = body;
    return addDictionaryType(dictionaryType);
  }
  update(body) {
    const dictionaryType = body;
    return updateDictionaryType(dictionaryType);
  }
  remove(body) {
    const { id } = body;
    return deleteDictionaryType(id);
  }
}
module.exports = new DictionaryTypeController();
