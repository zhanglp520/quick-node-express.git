const {
  getPermissionUserListByRoleId,
  getPermissionMenuListByRoleId,
  getRoleList,
  addRole,
  updateRole,
  deleteRole,
  assignPermission,
  assignUserPermission
} = require("../services/roleServices");
class RoleController {
  constructor() {}
  getUserPermission(body){
    const {id:roleId}=body
    return getPermissionUserListByRoleId(roleId)
  }
  getMenuPermission(body){
    const {id:roleId}=body
    return getPermissionMenuListByRoleId(roleId)
  }
  getList() {
    return getRoleList();
  }
  add(body) {
    const role = body;
    return addRole(role);
  }
  update(body) {
    const role = body;
    return updateRole(role);
  }
  remove(body) {
    const { id } = body;
    return deleteRole(id);
  }
  assignMenu(body) {
    const { roleId,menuIds } = body;
    return assignPermission(roleId,menuIds);
  }
  assignUser(body) {
    const { roleId,userIds } = body;
    return assignUserPermission(roleId,userIds);
  }
}
module.exports = new RoleController();
