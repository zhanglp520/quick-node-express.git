-- MySQL dump 10.13  Distrib 8.0.24, for Linux (x86_64)
--
-- Host: localhost    Database: quick
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `per_role_menus`
--

DROP TABLE IF EXISTS `per_role_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `per_role_menus` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int NOT NULL COMMENT '角色编号',
  `menu_id` int NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `per_role_menus`
--

LOCK TABLES `per_role_menus` WRITE;
/*!40000 ALTER TABLE `per_role_menus` DISABLE KEYS */;
INSERT INTO `per_role_menus` VALUES (316,1,13),(317,1,8),(318,1,9),(319,1,10),(320,1,11),(321,1,12),(322,1,4),(323,1,5),(324,1,6),(325,1,7),(326,1,24),(327,1,25),(328,1,16),(329,1,32),(330,1,29),(331,1,28),(332,1,30),(333,1,31),(334,1,15),(335,2,32),(336,2,29),(337,2,28),(338,2,30),(339,2,31),(340,3,7),(341,3,8),(342,3,9),(343,3,10),(344,3,11),(345,3,12),(346,3,13),(347,3,4),(348,3,5),(349,3,6),(350,3,24),(351,3,25),(352,3,32),(353,3,29),(354,3,30),(355,3,31),(356,3,28);
/*!40000 ALTER TABLE `per_role_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `per_user_roles`
--

DROP TABLE IF EXISTS `per_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `per_user_roles` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int NOT NULL COMMENT '用户编号',
  `role_id` int NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `per_user_roles`
--

LOCK TABLES `per_user_roles` WRITE;
/*!40000 ALTER TABLE `per_user_roles` DISABLE KEYS */;
INSERT INTO `per_user_roles` VALUES (12,11,1),(13,68,2),(14,73,3);
/*!40000 ALTER TABLE `per_user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_buttons`
--

DROP TABLE IF EXISTS `sys_buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_buttons` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `button_id` int NOT NULL COMMENT '按钮编号',
  `button_name` int NOT NULL COMMENT '按钮名称',
  `menu_id` int NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_buttons`
--

LOCK TABLES `sys_buttons` WRITE;
/*!40000 ALTER TABLE `sys_buttons` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_buttons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_depts`
--

DROP TABLE IF EXISTS `sys_depts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_depts` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dept_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门名称',
  `p_id` int DEFAULT NULL COMMENT '父级部门id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_depts`
--

LOCK TABLES `sys_depts` WRITE;
/*!40000 ALTER TABLE `sys_depts` DISABLE KEYS */;
INSERT INTO `sys_depts` VALUES (20,'BM_0001','集团',0),(56,'ainiteam','艾尼科技',20),(57,'Sales ','销售部门',56),(58,'development ','开发部',56),(59,'test','测试公司',20);
/*!40000 ALTER TABLE `sys_depts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dictionaries`
--

DROP TABLE IF EXISTS `sys_dictionaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dictionaries` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `dic_type_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `dic_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典编号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dictionaries`
--

LOCK TABLES `sys_dictionaries` WRITE;
/*!40000 ALTER TABLE `sys_dictionaries` DISABLE KEYS */;
INSERT INTO `sys_dictionaries` VALUES (1,'gender','1','男'),(2,'gender','2','女'),(3,'keyword','3','有偿'),(4,'keyword','4','付费'),(5,'keyword','4','帮忙'),(6,'keyword','5','花钱'),(7,'keyword','6','掏钱'),(8,'keyword','7','有接单的'),(9,'keyword','8','谁接单'),(10,'keyword','9','人接单'),(11,'keyword','10','解决问题'),(12,'keyword','11','求助'),(13,'keyword','12','帮助'),(14,'keyword','13','帮忙'),(15,'keyword','14','联系我'),(17,'keyword','15','有可以做的技术');
/*!40000 ALTER TABLE `sys_dictionaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dictionaries_type`
--

DROP TABLE IF EXISTS `sys_dictionaries_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dictionaries_type` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `dic_type_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型编码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dictionaries_type`
--

LOCK TABLES `sys_dictionaries_type` WRITE;
/*!40000 ALTER TABLE `sys_dictionaries_type` DISABLE KEYS */;
INSERT INTO `sys_dictionaries_type` VALUES (1,'gender','性别'),(2,'keyword','关键字');
/*!40000 ALTER TABLE `sys_dictionaries_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_logs`
--

DROP TABLE IF EXISTS `sys_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_logs` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `log_time` datetime NOT NULL COMMENT '日志时间',
  `operate_api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作接口',
  `request_params` json DEFAULT NULL COMMENT '请求参数',
  `error_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '错误信息',
  `exception_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '异常信息',
  `log_type` int DEFAULT NULL COMMENT '日志类型：0：操作日志，1：错误日志',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ip地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4324 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_logs`
--

LOCK TABLES `sys_logs` WRITE;
/*!40000 ALTER TABLE `sys_logs` DISABLE KEYS */;
INSERT INTO `sys_logs` VALUES (4063,'2022-08-29 17:54:35','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4064,'2022-08-29 17:54:35','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4065,'2022-08-29 17:54:36','/api/role/getUserPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4066,'2022-08-29 17:54:36','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4067,'2022-08-29 17:54:37','/api/role/getUserPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4068,'2022-08-29 17:54:37','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4069,'2022-08-29 17:54:37','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4070,'2022-08-29 17:54:37','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4071,'2022-08-29 17:54:47','/api/role/assignUser','{\"body\": {\"roleId\": 1, \"userIds\": \"11\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4072,'2022-08-29 17:54:49','/api/role/getUserPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4073,'2022-08-29 17:54:49','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4074,'2022-08-29 17:54:53','/api/role/assignUser','{\"body\": {\"roleId\": 2, \"userIds\": \"68\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4075,'2022-08-29 17:54:55','/api/role/getUserPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4076,'2022-08-29 17:54:55','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4077,'2022-08-29 17:54:56','/api/role/getUserPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4078,'2022-08-29 17:54:56','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4079,'2022-08-29 17:54:59','/api/role/assignUser','{\"body\": {\"roleId\": 3, \"userIds\": \"73\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4080,'2022-08-29 17:55:03','/api/role/getMenuPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4081,'2022-08-29 17:55:04','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4082,'2022-08-29 17:55:05','/api/role/getMenuPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4083,'2022-08-29 17:55:06','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4084,'2022-08-29 17:55:25','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4085,'2022-08-29 17:55:25','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4086,'2022-08-29 17:55:25','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4087,'2022-08-29 17:55:25','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4088,'2022-08-29 17:55:26','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4089,'2022-08-29 17:55:26','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4090,'2022-08-29 18:01:56','/api/menu/delete','{\"body\": {\"id\": 27}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4091,'2022-08-29 18:01:56','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4092,'2022-08-29 18:02:12','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4093,'2022-08-29 18:02:12','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4094,'2022-08-29 18:02:12','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4095,'2022-08-29 18:02:12','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4096,'2022-08-29 18:02:12','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4097,'2022-08-29 18:02:34','/api/role/getMenuPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4098,'2022-08-29 18:02:35','/api/role/getMenuPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4099,'2022-08-29 18:02:36','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4100,'2022-08-29 18:02:44','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4101,'2022-08-29 18:02:44','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4102,'2022-08-29 18:02:45','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4103,'2022-08-29 18:02:45','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4104,'2022-08-29 18:02:45','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4105,'2022-08-29 18:02:45','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4106,'2022-08-29 18:02:46','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4107,'2022-08-29 18:02:57','/api/role/assignPermission','{\"body\": {\"roleId\": 1, \"menuIds\": \"8,9,10,11,12,13,4,5,6,7,24,25,15,16,32,29,28,30,31\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4108,'2022-08-29 18:02:59','/api/role/getMenuPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4109,'2022-08-29 18:03:06','/api/role/assignPermission','{\"body\": {\"roleId\": 2, \"menuIds\": \"32,29,28,30,31\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4110,'2022-08-29 18:03:07','/api/role/getMenuPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4111,'2022-08-29 18:03:08','/api/role/getMenuPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4112,'2022-08-29 18:03:08','/api/role/getMenuPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4113,'2022-08-29 18:03:22','/api/role/assignPermission','{\"body\": {\"roleId\": 3, \"menuIds\": \"8,9,10,11,12,13,4,5,6,7,24,25,32,29,28,30,31\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4114,'2022-08-29 18:03:23','/api/role/getMenuPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4115,'2022-08-29 18:03:26','/api/role/getMenuPermission?id=3','{\"body\": {}, \"query\": {\"id\": \"3\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4116,'2022-08-29 18:03:27','/api/role/getMenuPermission?id=2','{\"body\": {}, \"query\": {\"id\": \"2\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4117,'2022-08-29 18:03:28','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4118,'2022-08-29 18:03:37','/api/login','{\"body\": {\"tenant\": \"\", \"userName\": \"admin\", \"userPassword\": \"e10adc3949ba59abbe56e057f20f883e\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4119,'2022-08-29 18:03:37','/api/user/getInfo?userName=admin','{\"body\": {}, \"query\": {\"userName\": \"admin\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4120,'2022-08-29 18:03:37','/api/user/getPermission?id=11','{\"body\": {}, \"query\": {\"id\": \"11\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4121,'2022-08-29 18:03:50','/api/login','{\"body\": {\"tenant\": \"\", \"userName\": \"user\", \"userPassword\": \"e10adc3949ba59abbe56e057f20f883e\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4122,'2022-08-29 18:03:50','/api/user/getInfo?userName=user','{\"body\": {}, \"query\": {\"userName\": \"user\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4123,'2022-08-29 18:03:50','/api/user/getPermission?id=68','{\"body\": {}, \"query\": {\"id\": \"68\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4124,'2022-08-29 18:04:01','/api/login','{\"body\": {\"tenant\": \"\", \"userName\": \"test\", \"userPassword\": \"e10adc3949ba59abbe56e057f20f883e\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4125,'2022-08-29 18:04:01','/api/user/getInfo?userName=test','{\"body\": {}, \"query\": {\"userName\": \"test\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4126,'2022-08-29 18:04:01','/api/user/getPermission?id=73','{\"body\": {}, \"query\": {\"id\": \"73\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4127,'2022-08-29 18:04:40','/api/login','{\"body\": {\"tenant\": \"\", \"userName\": \"admin\", \"userPassword\": \"e10adc3949ba59abbe56e057f20f883e\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4128,'2022-08-29 18:04:40','/api/user/getInfo?userName=admin','{\"body\": {}, \"query\": {\"userName\": \"admin\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4129,'2022-08-29 18:04:40','/api/user/getPermission?id=11','{\"body\": {}, \"query\": {\"id\": \"11\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4130,'2022-08-29 18:04:52','/api/login','{\"body\": {\"tenant\": \"\", \"userName\": \"admin\", \"userPassword\": \"e10adc3949ba59abbe56e057f20f883e\"}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4131,'2022-08-29 18:04:52','/api/user/getInfo?userName=admin','{\"body\": {}, \"query\": {\"userName\": \"admin\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4132,'2022-08-29 18:04:52','/api/user/getPermission?id=11','{\"body\": {}, \"query\": {\"id\": \"11\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4133,'2022-08-29 18:05:00','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4134,'2022-08-29 18:05:00','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4135,'2022-08-29 18:05:01','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4136,'2022-08-29 18:05:01','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4137,'2022-08-29 18:05:01','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4138,'2022-08-29 18:05:01','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4139,'2022-08-29 18:05:02','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4140,'2022-08-29 18:05:02','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4141,'2022-08-29 18:05:02','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4142,'2022-08-29 18:05:02','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4143,'2022-08-29 18:05:02','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4144,'2022-08-29 18:05:03','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4145,'2022-08-29 18:05:03','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4146,'2022-08-29 18:05:04','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4147,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4148,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4149,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4150,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4151,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4152,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4153,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4154,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4155,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4156,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4157,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4158,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4159,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4160,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4161,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4162,'2022-08-29 18:05:05','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4163,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4164,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4165,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4166,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4167,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4168,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4169,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4170,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4171,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4172,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4173,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4174,'2022-08-29 18:05:05','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4175,'2022-08-29 18:05:06','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4176,'2022-08-29 18:05:06','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4177,'2022-08-29 18:05:06','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4178,'2022-08-29 18:05:06','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4179,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4180,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4181,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4182,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4183,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4184,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4185,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4186,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4187,'2022-08-29 18:05:10','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4188,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4189,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4190,'2022-08-29 18:05:10','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4191,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4192,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4193,'2022-08-29 18:05:10','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4194,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4195,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4196,'2022-08-29 18:05:10','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4197,'2022-08-29 18:05:10','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4198,'2022-08-29 18:05:10','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4199,'2022-08-29 18:05:10','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4200,'2022-08-29 18:05:11','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4201,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4202,'2022-08-29 18:05:11','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4203,'2022-08-29 18:05:11','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4204,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4205,'2022-08-29 18:05:11','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4206,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4207,'2022-08-29 18:05:11','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4208,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4209,'2022-08-29 18:05:11','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4210,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4211,'2022-08-29 18:05:11','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4212,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4213,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4214,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4215,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4216,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4217,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4218,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4219,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4220,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4221,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4222,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4223,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4224,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4225,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4226,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4227,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4228,'2022-08-29 18:05:11','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4229,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4230,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4231,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4232,'2022-08-29 18:05:11','/api/role/getUserPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4233,'2022-08-29 18:05:11','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4234,'2022-08-29 18:05:11','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4235,'2022-08-29 18:05:11','/api/user/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4236,'2022-08-29 18:05:11','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4237,'2022-08-29 18:05:11','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4238,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4239,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4240,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4241,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4242,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4243,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4244,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4245,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4246,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4247,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4248,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4249,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4250,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4251,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4252,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4253,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4254,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4255,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4256,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4257,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4258,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4259,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4260,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4261,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4262,'2022-08-29 18:05:12','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4263,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4264,'2022-08-29 18:05:12','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4265,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4266,'2022-08-29 18:05:12','/api/role/getMenuPermission?id=1','{\"body\": {}, \"query\": {\"id\": \"1\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4267,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4268,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4269,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4270,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4271,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4272,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4273,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4274,'2022-08-29 18:05:15','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4275,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4276,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4277,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4278,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4279,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4280,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4281,'2022-08-29 18:05:18','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4282,'2022-08-29 18:05:26','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4283,'2022-08-29 18:05:26','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4284,'2022-08-29 18:05:26','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4285,'2022-08-29 18:05:26','/api/menu/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4286,'2022-08-29 18:05:29','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4287,'2022-08-29 18:05:29','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4288,'2022-08-29 18:05:29','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4289,'2022-08-29 18:05:29','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4290,'2022-08-29 18:05:29','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4291,'2022-08-29 18:05:29','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4292,'2022-08-29 18:05:29','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4293,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4294,'2022-08-29 18:05:30','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4295,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4296,'2022-08-29 18:05:30','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4297,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4298,'2022-08-29 18:05:30','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4299,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4300,'2022-08-29 18:05:30','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4301,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4302,'2022-08-29 18:05:30','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4303,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4304,'2022-08-29 18:05:30','/api/dept/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4305,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4306,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4307,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4308,'2022-08-29 18:05:30','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4309,'2022-08-29 18:05:30','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4310,'2022-08-29 18:05:31','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4311,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4312,'2022-08-29 18:05:31','/api/dictionaryType/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4313,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=','{\"body\": {}, \"query\": {\"dicTypeId\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4314,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4315,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4316,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4317,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4318,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4319,'2022-08-29 18:05:31','/api/dictionary/getList?dicTypeId=gender','{\"body\": {}, \"query\": {\"dicTypeId\": \"gender\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4320,'2022-08-29 18:05:58','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4321,'2022-08-29 18:05:59','/api/user/getPageList?userName=&current=1&size=10','{\"body\": {}, \"query\": {\"size\": \"10\", \"current\": \"1\", \"userName\": \"\"}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4322,'2022-08-29 18:05:59','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1'),(4323,'2022-08-29 18:05:59','/api/role/getList','{\"body\": {}, \"query\": {}}',NULL,NULL,0,'::ffff:127.0.0.1');
/*!40000 ALTER TABLE `sys_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menus`
--

DROP TABLE IF EXISTS `sys_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menus` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编号',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `path` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由',
  `view_path` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '视图',
  `menu_type` int NOT NULL COMMENT '菜单类型：0：目录，1：菜单，2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
  `sort` int NOT NULL COMMENT '排序',
  `btns` json DEFAULT NULL COMMENT '按钮',
  `p_id` int NOT NULL COMMENT '父id',
  `link` int NOT NULL COMMENT '是否外链：0：否，1：是',
  `link_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '链接地址',
  `enabled` int NOT NULL COMMENT '是否启用：0：启用，1：禁用',
  `status` int NOT NULL COMMENT '是否显示：0：显示，1：隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menus`
--

LOCK TABLES `sys_menus` WRITE;
/*!40000 ALTER TABLE `sys_menus` DISABLE KEYS */;
INSERT INTO `sys_menus` VALUES (1,'systemManager','系统管理','/system','',0,'user',1,NULL,0,0,'',0,1),(2,'userManager','用户管理','/system/user','',1,'user-filled',2,NULL,1,0,'',1,1),(3,'roleManager','角色管理','/system/role','',1,'Apple',3,NULL,1,0,'',1,1),(4,'menuManager','菜单管理','/system/menu','',1,'BellFilled',4,NULL,1,0,'',1,1),(5,'deptManager','部门管理','/system/dept','',1,'Bicycle',5,NULL,1,0,'',1,1),(6,'dictionaryType','字典分类','/system/dictionaryType','',1,'Collection',5,NULL,1,0,'',1,1),(7,'dictionaryManager','字典管理','/system/dictionary','',1,'Link',7,NULL,1,0,'',1,1),(8,'userAdd','新增',NULL,'',2,NULL,1,NULL,2,0,'',1,1),(9,'userDel','删除',NULL,'',2,NULL,2,NULL,2,0,'',1,1),(10,'userEdit','编辑',NULL,'',2,NULL,3,NULL,2,0,'',1,1),(11,'roleAdd','新增',NULL,'',2,NULL,1,NULL,3,0,'',1,1),(12,'roleDel','删除',NULL,'',2,NULL,2,NULL,3,0,'',1,1),(13,'roleEdit','编辑',NULL,'',2,NULL,3,NULL,3,0,'',1,1),(14,'permission','权限管理','/permission','',0,'Cloudy',2,NULL,0,0,'',1,1),(15,'assignUser','分配用户','/permission/assignUser','',1,'Filter',1,NULL,14,0,'',1,1),(16,'rolePermission','角色权限','/permission/rolePermission','',1,'CircleCheckFilled',2,NULL,14,0,'',1,1),(23,'logManager','日志管理','/system/log','',0,'Notification',8,NULL,1,0,'',1,1),(24,'operate','操作日志','/system/log/operate','/system/log/operate',1,'Edit',1,NULL,23,0,'',1,1),(25,'error','错误日志','/system/log/error','/system/log/error',1,'Download',2,NULL,23,0,'',1,1),(26,'helper','相关帮助','/helper','',0,'Food',3,NULL,0,0,'',1,1),(28,'gitee','码云','gitee','',1,'Watch',3,NULL,26,1,'https://gitee.com/zhanglp520/quick-vue3-admin',1,1),(29,'apiDocument','接口文档','','',1,'Aim',2,NULL,26,1,'https://console-docs.apipost.cn/preview/0e11a2eb3c3883a7/4fff7a394c074ac7',1,1),(30,'github','github','','',1,'Clock',4,NULL,26,1,'https://github.com/zhanglp520/quick-vue3-admin..git',1,1),(31,'gitlab','gitlab','','',1,'Chicken',5,NULL,26,1,'http://110.42.130.88:8099/zhanglp520/quick-vue3-admin.git',1,1),(32,'developDocument','开发文档','','',1,'Apple',0,NULL,26,1,'https://doc.ainiteam.com/',1,1);
/*!40000 ALTER TABLE `sys_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_roles`
--

DROP TABLE IF EXISTS `sys_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_roles` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编号',
  `role_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_roles`
--

LOCK TABLES `sys_roles` WRITE;
/*!40000 ALTER TABLE `sys_roles` DISABLE KEYS */;
INSERT INTO `sys_roles` VALUES (1,'admin','管理员'),(2,'user','普通用户'),(3,'test','测试专员');
/*!40000 ALTER TABLE `sys_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_users`
--

DROP TABLE IF EXISTS `sys_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_users` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户编号',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `avatar` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `full_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '备注',
  `deleted` int NOT NULL COMMENT '是否删除：0：未删除，1：已删除',
  `enabled` int NOT NULL COMMENT '是否启用：0：启用，1：禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_users`
--

LOCK TABLES `sys_users` WRITE;
/*!40000 ALTER TABLE `sys_users` DISABLE KEYS */;
INSERT INTO `sys_users` VALUES (11,'YH_0001','admin','14e1b600b1fd579f47433b88e8d85291','','管理员','15229380174','','西安','2022-05-13 08:20:47','管理员账号误删',0,0),(68,'YH_0002','user','14e1b600b1fd579f47433b88e8d85291',NULL,'用户',NULL,NULL,NULL,'2022-08-07 12:13:58','',0,0),(73,'YH_0003','test','14e1b600b1fd579f47433b88e8d85291',NULL,'','','','','2022-08-11 09:45:02','',0,0);
/*!40000 ALTER TABLE `sys_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'quick'
--

--
-- Dumping routines for database 'quick'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-29 18:06:28
